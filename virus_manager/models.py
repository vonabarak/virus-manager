# -*- coding: utf-8 -*-

from django.db import models
# from django.contrib.auth import get_user_model
from django.conf import settings
import json


def date_or_none_to_iso(d):
    if d is None:
        return None
    else:
        return d.isoformat()


class Host(models.Model):
    hostname = models.CharField(max_length=128, unique=True)
    ctime = models.DateTimeField(auto_now_add=True)
    check_started = models.DateTimeField(null=True)
    check_finished = models.DateTimeField(null=True)

    @property
    def is_scanning(self):
        if (self.check_finished is None and self.check_started is not None) or \
                (self.check_started > self.check_finished):
            return True
        else:
            return False

    def __unicode__(self):
        return self.hostname

    def as_json(self):
        return json.dumps({
            "id": self.id,
            "hostname": self.hostname
        })


class User(models.Model):
    host = models.ForeignKey(Host, related_name='users')
    username = models.CharField(max_length=128)
    check_started = models.DateTimeField(null=True)
    check_finished = models.DateTimeField(null=True)

    class Meta:
        unique_together = ('username', 'host')

    @property
    def is_scanning(self):
        if (self.check_finished is None and self.check_started is not None) or \
                (self.check_started > self.check_finished):
            return True
        else:
            return False

    def __unicode__(self):
        return u'{0}@{1}'.format(self.username, self.host.hostname)

    def as_json(self):
        return json.dumps({
            "id": self.id,
            "host": self.host_id,
            "username": self.username,
            "check_started": date_or_none_to_iso(self.check_started),
            "check_finished": date_or_none_to_iso(self.check_finished)
        })


class Virus(models.Model):
    virusname = models.CharField(max_length=128)
    ignoring = models.BooleanField(default=False)
    database = models.CharField(max_length=128, null=True)

    def __unicode__(self):
        return self.virusname

    def as_json(self):
        return json.dumps({
            "id": self.id,
            "virusname": self.virusname,
            "ignoring": self.ignoring,
            "database": self.database
        })


class File(models.Model):
    user = models.ForeignKey(User, related_name='files')
    filename = models.CharField(max_length=255)
    virus = models.ForeignKey(Virus, related_name='files')
    ignoring = models.BooleanField(default=False)
    quarantine = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    permissions = models.CharField(max_length=32, null=True)
    ctime = models.DateTimeField(auto_now_add=True)
    atime = models.DateTimeField(auto_now=True)
    etime = models.DateTimeField(null=True)

    class Meta:
        unique_together = ('user', 'filename')

    @property
    def ignore(self):
        if self.virus.ignore or self.ignore:
            return True
        else:
            return False

    def __unicode__(self):
        return self.filename

    def as_json(self):
        return json.dumps({
            "id": self.id,
            "user": self.user_id,
            "filename": self.filename,
            "virus": self.virus_id,
            "ignoring": self.ignoring,
            "quarantine": self.quarantine,
            "deleted": self.deleted,
            "permissions": self.permissions,
            "ctime": date_or_none_to_iso(self.ctime),
            "atime": date_or_none_to_iso(self.atime),
            "etime": date_or_none_to_iso(self.etime)
        })


class FileComment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='file_comments', null=True)
    file = models.ForeignKey(File, related_name='comments')
    comment = models.TextField()
    ticket = models.BigIntegerField(null=True)
    ctime = models.DateTimeField(auto_now_add=True)
    system = models.BooleanField(default=False)


class UserComment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_comments', null=True)
    user = models.ForeignKey(User, related_name='comments')
    comment = models.TextField()
    ticket = models.BigIntegerField(null=True)
    ctime = models.DateTimeField(auto_now_add=True)
    system = models.BooleanField(default=False)


class VirusComment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='virus_comments', null=True)
    virus = models.ForeignKey(Virus, related_name='comments')
    comment = models.TextField()
    ticket = models.BigIntegerField(null=True)
    ctime = models.DateTimeField(auto_now_add=True)
    system = models.BooleanField(default=False)


class AuthUserPrefs(models.Model):
    auth_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='preferences')
    rows_on_page = models.IntegerField(default=10)
    show_create_forms = models.BooleanField(default=False)
