# -*- coding: utf-8 -*-

import json
import logging
from datetime import datetime
from django.views.generic import FormView
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import CreateView, UpdateView
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.db.models import Q, Count, Max
from django.contrib import messages
from django.contrib.auth import logout, login, authenticate
from django.http import \
    HttpResponseRedirect, HttpResponse, HttpResponseServerError, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from virus_manager.models import *
from virus_manager.forms import *
from virus_manager.queue import publish

logger = logging.getLogger('debug')


class HomePageView(TemplateView):
    template_name = 'home.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        # messages.info(self.request, 'This is a demo of a message.')
        context['new_files'] = File.objects.filter(
            deleted=False,
            quarantine=False,
            ignoring=False,
            etime=None,
            virus__ignoring=False
        ).order_by('-atime')[:10]
        context['new_viruses'] = Virus.objects.filter(ignoring=False).order_by('-id')[:10]
        context['top_viruses'] = \
            Virus.objects.filter(ignoring=False).annotate(files_count=Count('files')).order_by('-files_count')[:10]
        context['top_users'] = \
            User.objects.raw("""SELECT u.id, COUNT(f.id) as files_count FROM virus_manager_user u
                                  JOIN virus_manager_file f on f.user_id=u.id
                                  JOIN virus_manager_virus v on f.virus_id=v.id
                                  WHERE f.deleted=FALSE
                                  AND f.ignoring=FALSE
                                  AND f.quarantine=FALSE
                                  AND v.ignoring=FALSE
                                  AND f.etime IS NULL
                                  GROUP BY u.id
                                  ORDER BY files_count DESC LIMIT 10""")

        # User.objects.all().annotate(files_count=Count('files')).order_by('-files_count')[:10]
        context['infected_files'] = File.objects.filter(
            deleted=False,
            ignoring=False,
            virus__ignoring=False,
            etime=None
        ).count()
        context['infected_users'] = User.objects.filter(
            files__ignoring=False,
            files__virus__ignoring=False,
            files__deleted=False
        ).count()
        context['quarantined_files'] = File.objects.filter(
            deleted=False,
            ignoring=False,
            etime=None,
            virus__ignoring=False,
            quarantine=True
        ).count()
        context['ignored_files'] = File.objects.filter(Q(ignoring=True) | Q(virus__ignoring=True)).__len__()
        context['all_viruses'] = Virus.objects.all().count()
        context['ignored_viruses'] = Virus.objects.filter(ignoring=True).count()
        return context


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context

    def post(self, request, **kwargs):
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.info(request, 'Успешный вход в систему')
            else:
                messages.info(request, 'Аккаунт отключён')
        else:
            messages.warning(request, 'Неверный логин/пароль')
        redirect = request.GET.get('next', False)
        if redirect:
            return HttpResponseRedirect(redirect)
        else:
            # return render(request, self.template_name, self.get_context_data(**kwargs))
            return HttpResponseRedirect(reverse('home'))


class LogoutView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        messages.info(request, 'До свидания!')
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class PaginationMixin:
    def __init__(self):
        self.lines = list()
        self.request = None

    def add_paginator(self, context):
        (prefs, prefs_created) = AuthUserPrefs.objects.get_or_create(auth_user=self.request.user)
        rows = self.request.GET.get('rows', prefs.rows_on_page)
        paginator = Paginator(self.lines, rows)
        page = self.request.GET.get('page')
        try:
            show_lines = paginator.page(page)
        except PageNotAnInteger:
            show_lines = paginator.page(1)
        except EmptyPage:
            show_lines = paginator.page(paginator.num_pages)
        context['lines'] = show_lines
        context['rows'] = rows
        context['page'] = page


class HostsView(CreateView, PaginationMixin):
    template_name = 'hosts.html'
    model = Host
    form_class = HostForm
    success_url = reverse_lazy('hosts')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        fltr_all = self.request.GET.get('fltr_all', '')
        if fltr_all:
            hosts = Host.objects.all().annotate(newest_file=Max('users__files__atime')).order_by('-newest_file')
        else:
            hosts = Host.objects.filter(
                users__files__deleted=False,
                users__files__ignoring=False,
                users__files__quarantine=False,
                users__files__virus__ignoring=False,
                users__files__etime=None
            ).annotate(newest_file=Max('users__files__atime')).order_by('-newest_file')

        fltr = self.request.GET.get('filter', '')
        if fltr:
            self.lines = hosts.filter(hostname__regex=fltr).order_by('hostname')
        else:
            self.lines = hosts
        context['fltr_all'] = fltr_all
        context['fltr'] = fltr
        self.add_paginator(context)
        return context


class UsersView(CreateView, PaginationMixin):
    template_name = 'users.html'
    model = User
    form_class = UserForm
    success_url = reverse_lazy('users')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        fltr_all = self.request.GET.get('fltr_all', '')
        if fltr_all:
            users = User.objects.all().annotate(newest_file=Max('files__atime')).order_by('-newest_file')
        else:
            users = User.objects.filter(
                files__deleted=False,
                files__ignoring=False,
                files__quarantine=False,
                files__virus__ignoring=False,
                files__etime=None
            ).annotate(newest_file=Max('files__atime')).order_by('-newest_file')

        fltr = self.request.GET.get('filter', '')
        if fltr:
            fields = ['username', 'host__hostname']
            queries = [Q(**{f+'__regex': fltr}) for f in fields]
            qs = Q()
            for query in queries:
                qs = qs | query
            self.lines = users.filter(qs)
        else:
            self.lines = users
        context['fltr_all'] = fltr_all
        context['fltr'] = fltr
        self.add_paginator(context)
        return context


class VirusesView(CreateView, PaginationMixin):
    template_name = 'viruses.html'
    model = Virus
    form_class = VirusForm
    success_url = reverse_lazy('viruses')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        fltr_all = self.request.GET.get('fltr_all', '')
        if fltr_all:
            viruses = Virus.objects.all().order_by('virusname')
        else:
            viruses = Virus.objects.filter(ignoring=False).order_by('virusname')
        fltr = self.request.GET.get('filter', '')
        if fltr:
            self.lines = viruses.filter(virusname__regex=fltr).order_by('virusname')
        else:
            self.lines = viruses
        context['fltr_all'] = fltr_all
        context['fltr'] = fltr
        self.add_paginator(context)
        return context


class FilesView(CreateView, PaginationMixin):
    template_name = 'files.html'
    model = File
    form_class = FileForm
    success_url = reverse_lazy('files')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        fltr_all = self.request.GET.get('fltr_all', '')
        if fltr_all:
            files = File.objects.all().order_by('-atime')
        else:
            files = File.objects.filter(
                deleted=False,
                ignoring=False,
                quarantine=False,
                virus__ignoring=False,
                etime=None
            ).order_by('-atime')

        fltr = self.request.GET.get('filter', '')
        if fltr:
            fields = ['filename', 'user__username', 'user__host__hostname', 'virus__virusname']
            queries = [Q(**{f+'__regex': fltr}) for f in fields]
            qs = Q()
            for query in queries:
                qs = qs | query
            self.lines = files.filter(qs).order_by('-atime')
        else:
            self.lines = files
        context['fltr_all'] = fltr_all
        context['fltr'] = fltr
        self.add_paginator(context)
        return context


class UserDetailView(FormView, TemplateView, PaginationMixin):
    template_name = 'user_details.html'
    form_class = AddCommentForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, user_id, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        user = get_object_or_404(User, id=int(user_id))
        context['vmuser'] = user
        context['comments'] = user.comments.all().order_by('-ctime')
        self.lines = user.files.all().order_by('-ctime')
        self.add_paginator(context)
        # context['user_files'] = File.objects.filter(
        #     deleted=False,
        #     quarantine=False,
        #     ignoring=False,
        #     etime=None,
        #     virus__ignoring=False
        # ).order_by('-atime')
        form = context['form']
        if form.is_bound and form.is_valid():
            ticket = form['ticket'].value()
            if not ticket:
                ticket = None
            UserComment(
                user=user,
                comment=form['comment'].value(),
                ticket=ticket,
                author=self.request.user
            ).save()
            context['form'] = self.get_form_class()()
        return context

    def post(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(*args, **kwargs))


class FileDetailView(FormView, TemplateView):
    template_name = 'file_details.html'
    form_class = AddCommentForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, file_id, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        file_ = get_object_or_404(File, id=int(file_id))
        context['file'] = file_
        context['comments'] = file_.comments.all().order_by('-ctime')
        form = context['form']
        if form.is_bound and form.is_valid():
            ticket = form['ticket'].value()
            if not ticket:
                ticket = None
            FileComment(
                file=file_,
                comment=form['comment'].value(),
                ticket=ticket,
                author=self.request.user
            ).save()
            context['form'] = self.get_form_class()()
        return context

    def post(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(*args, **kwargs))


class VirusDetailView(FormView, TemplateView):
    template_name = 'virus_details.html'
    form_class = AddCommentForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, virus_id, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        virus = get_object_or_404(Virus, id=int(virus_id))
        context['virus'] = virus
        context['comments'] = virus.comments.all().order_by('-ctime')
        form = context['form']
        if form.is_bound and form.is_valid():
            ticket = form['ticket'].value()
            if not ticket:
                ticket = None
            VirusComment(
                virus=virus,
                comment=form['comment'].value(),
                ticket=ticket,
                author=self.request.user
            ).save()
            context['form'] = self.get_form_class()()
        return context

    def post(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(*args, **kwargs))


class AuthUserPrefsView(UpdateView):
    template_name = 'auth_user_prefs.html'
    form_class = AuthUserPrefsForm
    model = AuthUserPrefs
    context_object_name = 'prefs'
    success_url = reverse_lazy('auth_prefs')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        (prefs, prefs_created) = AuthUserPrefs.objects.get_or_create(auth_user=self.request.user)
        return prefs

    # def get_context_data(self, **kwargs):
    #     context = super(self.__class__, self).get_context_data(**kwargs)
    #     (prefs, prefs_created) = AuthUserPrefs.objects.get_or_create(auth_user=self.request.user)
    #     context['form'] = self.get_form_class()()
    #     context['prefs'] = prefs


class HelpView(TemplateView):
    template_name = 'help.html'


class ActionView(View):
    @method_decorator(csrf_protect)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def post(self, request, cathegory):
        try:
            d = json.loads(request.read())
        except ValueError:
            logger.warn('Cant parse request as JSON')
            return HttpResponseServerError('Cant parse request as JSON')
        for m in d.keys():
            try:
                self.__getattribute__(cathegory + '_' + m)
            except AttributeError:
                logger.warn('Unknown method of json-api {0}'.format(cathegory + '_' + m))
                return HttpResponseBadRequest('Unknown method {0}'.format(cathegory + '_' + m))

        def action(m, k, v):
            try:
                f = File.objects.get(id=k)
                comment = self.__getattribute__(cathegory + '_' + m)(f, v)
                FileComment(
                    author=request.user,
                    file=f,
                    comment=comment,
                    system=True
                ).save()
            except AttributeError:
                logger.warn('Unknown method of json-api {0}'.format(cathegory + '_' + m))
                return None
            except BaseException as e:
                logger.warn('Cant change ignoring state of file {0}: {1}'.format(k, e))
                return None

        return HttpResponse(
            json.dumps(
                {m: {k: action(m, k, v) for k, v in x.items()} for m, x in d.items()}
            )
        )

    @staticmethod
    def file_ignoring(f, v):
        f.ignoring = v
        f.save()
        if v:
            return 'Игнорировать файл'
        else:
            return 'Не игнорировать файл'

    @staticmethod
    def file_quarantine(f, v):
        if v:
            comment = 'Карантин вкл.'
            method = 'chmod_file'
        else:
            comment = 'Карантин выкл.'
            method = 'unchmod_file'
        publish(f.user.host.hostname, method, json.dumps(
            {
                'hostname': f.user.host.hostname,
                'username': f.user.username,
                'filename': f.filename
            }
        ))
        return comment

    @staticmethod
    def file_delete(f, v):
        if v:
            publish(f.user.host.hostname, 'delete_file', json.dumps(
                {
                    'hostname': f.user.host.hostname,
                    'username': f.user.username,
                    'filename': f.filename
                }
            ))
            return 'Удалить файл'


class CsvView(View):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get(self, request, cathegory):
        if cathegory == 'file':
            user_id = int(request.GET.get('user', '0'))
            user = get_object_or_404(User, id=user_id)
            files = user.files.filter(
                deleted=False,
                ignoring=False,
                quarantine=False,
                virus__ignoring=False,
                etime=None
            ).order_by('-atime')

            response = HttpResponse('FILENAME\t:\tVIRUS\n' + '\n'.join(map(
                lambda x: '{0}\t:\t{1}'.format(x.filename, x.virus.virusname),
                list(files)
            )))
            response['Content-Type'] = 'text/csv'
            response['Content-Disposition'] = 'attachment; filename="{0}_{1}.csv"'.format(
                datetime.now().strftime('%Y-%m-%d_%H-%M'),
                user.username
            )
            return response
        else:
            return HttpResponseNotFound()


class ApiView(View):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get(self, request, cathegory, unit_id):
        method = self.__getattribute__(cathegory + '_api')
        return HttpResponse(method(request, unit_id))

    @staticmethod
    def host_api(request, host_id):
        h = get_object_or_404(Host, id=int(host_id))
        scan = request.GET.get('scan')
        if scan:
            publish(
                h.hostname, 'full_scan', json.dumps(dict())
            )
        return h.as_json()

    @staticmethod
    def user_api(request, user_id):
        u = get_object_or_404(User, id=int(user_id))
        scan = request.GET.get('scan')
        if scan:
            # u.check_started = now()
            # u.save()
            UserComment(
                author=request.user,
                user=u,
                comment='Начать полное сканирование',
                system=True
            ).save()
            publish(
                u.host.hostname, 'scan', json.dumps(
                    {
                        'hostname': u.host.hostname,
                        'username': u.username

                    }
                )
            )
        return u.as_json()

    @staticmethod
    def virus_api(request, virus_id):
        v = get_object_or_404(Virus, id=int(virus_id))
        ignoring = request.GET.get('ignoring', '')
        if ignoring:
            v.ignoring = ignoring == 'true'
        v.save()
        if v.ignoring:
            comment = 'Игнорировать вирус'
        else:
            comment = 'Не игнорировать вирус'
        VirusComment(
            author=request.user,
            virus=v,
            comment=comment,
            system=True
        ).save()
        return v.as_json()

    @staticmethod
    def file_api(request, file_id):
        f = get_object_or_404(File, id=int(file_id))

        switch_ignoring = request.GET.get('ignoring', '')
        switch_quarantine = request.GET.get('quarantine', '')
        delete = request.GET.get('delete', '')
        comment = ''

        if switch_ignoring:
            f.ignoring = switch_ignoring == 'true'
            if f.ignoring:
                comment = 'Игнорировать файл'
            else:
                comment = 'Не игнорировать файл'
            f.save()
        if switch_quarantine:
            f.quarantine = switch_quarantine == 'true'
            if f.quarantine:
                method = 'chmod_file'
                comment = 'Карантин вкл.'
            else:
                method = 'unchmod_file'
                comment = 'Карантин выкл.'
            publish(f.user.host.hostname, method, json.dumps(
                {
                    'hostname': f.user.host.hostname,
                    'username': f.user.username,
                    'filename': f.filename,
                    'details': f.permissions
                }
            ))

        if delete:
            # f.deleted = True
            comment = 'Удалить файл'
            publish(f.user.host.hostname, 'delete_file', json.dumps(
                {
                    'hostname': f.user.host.hostname,
                    'username': f.user.username,
                    'filename': f.filename
                }
            ))
        FileComment(
            author=request.user,
            file=f,
            comment=comment,
            system=True
        ).save()
        return f.as_json()
