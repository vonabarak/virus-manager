# -*- coding: utf-8 -*-

from django.test import TestCase
from django.utils.timezone import now
from datetime import timedelta

from virus_manager.models import *
from virus_manager.callbacks import Callback


class CallbacksTest(TestCase):
    def setUp(self):
        self.callbacks = __import__('virus_manager.callbacks').callbacks

    def test_01_virus_found(self):
        # found a virus
        Callback.virus_found(
            virusname='test_virusname',
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=True
        )
        # all requested objects should be created
        self.assertEquals(Virus.objects.filter(virusname='test_virusname').count(), 1)
        self.assertEquals(Host.objects.filter(hostname='test_hostname').count(), 1)
        self.assertEquals(User.objects.filter(username='test_username').count(), 1)
        self.assertEquals(File.objects.filter(filename='test_filename').count(), 1)

        # found same virus in same file one more time
        Callback.virus_found(
            virusname='test_virusname',
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=True
        )
        # should be still only one instance of each object
        self.assertEquals(Virus.objects.filter(virusname='test_virusname').count(), 1)
        self.assertEquals(Host.objects.filter(hostname='test_hostname').count(), 1)
        self.assertEquals(User.objects.filter(username='test_username').count(), 1)
        self.assertEquals(File.objects.filter(filename='test_filename').count(), 1)

        # we got error
        Callback.virus_found(
            virusname='test_virusname',
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=False,
            details='Some sort of error occurred'
        )

        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        f = File.objects.get(filename='test_filename', user=u)
        self.assertEquals(
            FileComment.objects.filter(file=f).last().comment,
            'Some sort of error occurred'
        )

    def test_02_scanning_started(self):
        Callback.scanning_started(
            hostname='test_hostname',
            username='test_username',
            success=True
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        # may slightly differ as our now() was called after scanning_started,
        # so use assertAlmostEquals here
        self.assertAlmostEquals(
            u.check_started,
            now(),
            delta=timedelta(seconds=5)
        )
        self.assertEquals(
            UserComment.objects.filter(user=u).last().comment,
            'Scanning started'
        )
        # check for creating comment on error
        Callback.scanning_started(
            hostname='test_hostname',
            username='test_username',
            success=False,
            details='Something goes wrong'
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        self.assertEquals(
            UserComment.objects.filter(user=u).last().comment,
            'Something goes wrong'
        )

    def test_03_scanning_finished(self):
        # found a virus
        Callback.virus_found(
            virusname='test_virusname',
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
        )
        # then fullscan finished
        Callback.scanning_finished(
            hostname='test_hostname',
            username='test_username',
            success=True
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        f = File.objects.get(filename='test_filename', user=u)
        # file must be marked as cured
        self.assertIsNotNone(f.etime)
        # comment for user should be added
        self.assertEquals(
            UserComment.objects.filter(user=u).last().comment,
            'Scanning finished'
        )

        # check for creating comment on error
        Callback.scanning_finished(
            hostname='test_hostname',
            username='test_username',
            success=False,
            details='Something goes wrong'
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        self.assertEquals(
            UserComment.objects.filter(user=u).last().comment,
            'Something goes wrong'
        )

    def test_04_file_chmoded(self):
        (h, host_created) = Host.objects.get_or_create(hostname='test_hostname')
        (u, user_created) = User.objects.get_or_create(host=h, username='test_username')
        (v, virus_created) = Virus.objects.get_or_create(virusname='test_virus')
        (f, file_created) = File.objects.get_or_create(
            user=u,
            filename='test_filename',
            virus=v,
        )
        self.assertFalse(f.quarantine)
        Callback.file_chmoded(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        f = File.objects.get(filename='test_filename', user=u)
        self.assertTrue(f.quarantine)

        # check for creating comment on error
        Callback.file_chmoded(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=False,
            details='Something goes wrong'
        )
        self.assertEquals(
            FileComment.objects.filter(file=f).last().comment,
            'Something goes wrong'
        )

    def test_05_file_unchmoded(self):
        (h, host_created) = Host.objects.get_or_create(hostname='test_hostname')
        (u, user_created) = User.objects.get_or_create(host=h, username='test_username')
        (v, virus_created) = Virus.objects.get_or_create(virusname='test_virus')
        (f, file_created) = File.objects.get_or_create(
            user=u,
            filename='test_filename',
            virus=v,
        )
        self.assertFalse(f.quarantine)
        Callback.file_chmoded(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        f = File.objects.get(filename='test_filename', user=u)
        self.assertTrue(f.quarantine)

        # check for creating comment on error
        Callback.file_unchmoded(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=False,
            details='Something goes wrong'
        )
        self.assertEquals(
            FileComment.objects.filter(file=f).last().comment,
            'Something goes wrong'
        )

    def test_06_file_deleted(self):
        (h, host_created) = Host.objects.get_or_create(hostname='test_hostname')
        (u, user_created) = User.objects.get_or_create(host=h, username='test_username')
        (v, virus_created) = Virus.objects.get_or_create(virusname='test_virus')
        (f, file_created) = File.objects.get_or_create(
            user=u,
            filename='test_filename',
            virus=v,
        )
        self.assertFalse(f.deleted)
        Callback.file_deleted(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
        )
        h = Host.objects.get(hostname='test_hostname')
        u = User.objects.get(username='test_username', host=h)
        f = File.objects.get(filename='test_filename', user=u)
        self.assertTrue(f.deleted)

        # check for creating comment on error
        Callback.file_deleted(
            hostname='test_hostname',
            username='test_username',
            filename='test_filename',
            success=False,
            details='Something goes wrong'
        )
        self.assertEquals(
            FileComment.objects.filter(file=f).last().comment,
            'Something goes wrong'
        )
