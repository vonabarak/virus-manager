# -*- coding: utf-8 -*-

import os
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
os.environ.setdefault("LDAPTLS_REQCERT", "never")
from django_auth_ldap.backend import LDAPBackend


class VMLDAPBackend(LDAPBackend):

    def authenticate(self, username, password, **kwargs):
        try:
            get_user_model().objects.get(username=username)
        except ObjectDoesNotExist:
            pass
        res = LDAPBackend.authenticate(self, username, password, **kwargs)
        return res

    @staticmethod
    def get_or_create_user(username, ldap_user):
        user_model = get_user_model()
        return user_model.objects.get_or_create(username=username)
