# -*- coding: utf-8 -*-

from virus_manager.models import *
import logging
import json
from django.utils.timezone import now
from django.db.models import Q

logger = logging.getLogger(__name__)


class Callback(object):
    def __init__(self, channel, basic_deliver, properties, body):

        self.channel = channel
        self.basic_deliver = basic_deliver

        self.method = self.dummy
        self.method_name = 'dummy'
        self.params = dict()

        try:
            self.params = json.loads(body)
        except ValueError as e:
            logger.warn('Cannot parse message as json: {0}'.format(e))

        if 'method' not in properties.headers:
            logger.warn('Header "method" required')
        else:
            try:
                self.method = self.__getattribute__(properties.headers['method'])
                self.method_name = properties.headers['method']
            except AttributeError:
                logger.warn('Trying to call non-existent method {0}'.format(properties.headers['method']))

    def __call__(self):
        try:
            logger.debug('Calling method {0} with params {1}'.format(self.method_name, self.params))
            self.method(**self.params)
            return True
        except BaseException as e:
            logger.error('Exception "{0}" occured while executing method {1}'.format(e, self.method_name))
            return False

    @staticmethod
    def dummy(*args, **kwargs):
        logger.info('Executing dummy callback with args={0} kwargs={1}'.format(args, kwargs))

    @staticmethod
    def virus_found(virusname, hostname, username, filename, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        (virus, virus_created) = Virus.objects.get_or_create(virusname=virusname)
        (file_, file_created) = File.objects.get_or_create(
            user=user,
            filename=filename,
            virus=virus,
        )
        file_.deleted = False
        file_.quarantine = False
        file_.etime = None
        file_.save()
        if success is not None and not success:
            FileComment(
                file=file_,
                comment=details,
                system=True
            ).save()

    @staticmethod
    def scanning_started(hostname, username, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        user.check_started = now()
        user.save()
        if success is not None and not success:
            UserComment(
                user=user,
                comment=details,
                system=True
            ).save()
        else:
            UserComment(
                user=user,
                comment='Scanning started',
                system=True
            ).save()

    @staticmethod
    def scanning_finished(hostname, username, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        user.check_finished = now()
        user.save()
        if user.check_started is None:
            UserComment(
                user=user,
                comment='Got "Scanning finished" message while scanning wasn\'t started yet',
                system=True
            ).save()
        # set files with atime < check_started as cured
        for f in user.files.filter(Q(deleted=False) & Q(etime=None)):
            if not f.deleted and (user.check_started is None or f.atime < user.check_started):
                f.etime = now()
                f.save()
                FileComment(
                    file=f,
                    comment='File marked as cured because it wasn\'t detected by full scan',
                    system=True
                ).save()
        if success is not None and not success:
            UserComment(
                user=user,
                comment=details,
                system=True
            ).save()
        else:
            UserComment(
                user=user,
                comment='Scanning finished',
                system=True
            ).save()

    @staticmethod
    def file_chmoded(hostname, username, filename, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        try:
            file_ = File.objects.get(user=user, filename=filename)
            file_.quarantine = True
            if success:
                file_.permissions = details
            file_.save()
            if success is not None and not success:
                FileComment(
                    file=file_,
                    comment=details,
                    system=True
                ).save()
        except File.DoesNotExist:
            logger.warn('File not found: {0} {1}'.format(user, filename))

    @staticmethod
    def file_unchmoded(hostname, username, filename, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        try:
            file_ = File.objects.get(user=user, filename=filename)
            file_.quarantine = False
            file_.save()
            if success is not None and not success:
                FileComment(
                    file=file_,
                    comment=details,
                    system=True
                ).save()
        except File.DoesNotExist:
            logger.warn('File not found: {0} {1}'.format(user, filename))

    @staticmethod
    def file_deleted(hostname, username, filename, success=None, details=None):
        (host, host_created) = Host.objects.get_or_create(hostname=hostname)
        (user, user_created) = User.objects.get_or_create(host=host, username=username)
        try:
            file_ = File.objects.get(user=user, filename=filename)
            file_.etime = now()
            file_.deleted = True
            file_.save()
            if success is not None and not success:
                FileComment(
                    file=file_,
                    comment=details,
                    system=True
                ).save()
        except File.DoesNotExist:
            logger.warn('File not found: {0} {1}'.format(user, filename))
