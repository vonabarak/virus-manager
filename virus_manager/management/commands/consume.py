# -*- coding: utf-8 -*-

import signal
from django.core.management.base import BaseCommand
from virus_manager.queue import Consumer


class Command(BaseCommand):
    help = 'Start consuming messages from RabbitMQ queue'
    consumer = Consumer()

    def signal_handler(self, signal, frame):
        print('Stopping consumer')
        self.consumer.stop()
        self.consumer.close_connection()

    def handle(self, *args, **kwargs):
        signal.signal(signal.SIGINT, self.signal_handler)
        try:
            self.consumer.run()
        except IOError:
            pass