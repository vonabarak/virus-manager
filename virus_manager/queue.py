# -*- coding: utf-8 -*-
import pika
import threading
from django.conf import settings
import logging


class Publisher:
    def __init__(self, name):
        self.name = name
        self.connection = pika.BlockingConnection(
            pika.URLParameters(settings.BROKER_SETTINGS['url'])
        )
        self.channel = self.connection.channel()
        self.channel.queue_declare(
            queue=name,
            durable=True
        )
        self.channel.queue_bind(
            exchange=settings.BROKER_SETTINGS['exchange'],
            queue=name,
            routing_key=name
        )

    def __call__(self, message, *args, **kwargs):
        if 'properties' in kwargs:
            properties = kwargs['properties']
        else:
            properties = pika.BasicProperties()
        self.channel.basic_publish(
            exchange=settings.BROKER_SETTINGS['exchange'],
            routing_key=self.name,
            body=message,
            properties=properties
        )

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()


class Consumer(threading.Thread):
    exchange = settings.BROKER_SETTINGS['exchange']
    exchange_type = settings.BROKER_SETTINGS['exchange_type']
    queue = settings.BROKER_SETTINGS['queue']
    routing_key = settings.BROKER_SETTINGS['routing_key']

    def __init__(self):
        threading.Thread.__init__(self)
        self.logger = logging.getLogger(__name__)
        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._url = settings.BROKER_SETTINGS['url']

    def run(self):
        self.logger.info('Starting consumer thread...')
        self._connection = self.connect()
        self._connection.ioloop.start()

    def connect(self):
        self.logger.debug('Connecting to {0}'.format(self._url))
        return pika.SelectConnection(pika.URLParameters(self._url),
                                     self.on_connection_open,
                                     stop_ioloop_on_close=False)

    def on_connection_open(self, unused_connection):
        self.logger.debug('Connection opened')
        self.add_on_connection_close_callback()
        self.open_channel()

    def add_on_connection_close_callback(self):
        self.logger.debug('Adding connection close callback')
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reply_code, reply_text):
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self.logger.warning('Connection closed, reopening in 5 seconds: (%s) %s',
                           reply_code, reply_text)
            self._connection.add_timeout(5, self.reconnect)

    def reconnect(self):
        self._connection.ioloop.stop()
        if not self._closing:
            self._connection = self.connect()
            self._connection.ioloop.start()

    def open_channel(self):
        self.logger.debug('Creating a new channel')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        self.logger.debug('Channel opened')
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.exchange)

    def add_on_channel_close_callback(self):
        self.logger.debug('Adding channel close callback')
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reply_code, reply_text):
        self.logger.warning('Channel %i was closed: (%s) %s',
                       channel, reply_code, reply_text)
        self._connection.close()

    def setup_exchange(self, exchange_name):
        self.logger.debug('Declaring exchange %s', exchange_name)
        self._channel.exchange_declare(self.on_exchange_declareok,
                                       exchange_name,
                                       self.exchange_type,
                                       durable=True)

    def on_exchange_declareok(self, unused_frame):
        self.logger.debug('Exchange declared')
        self.setup_queue(self.queue)

    def setup_queue(self, queue_name):
        self.logger.debug('Declaring queue %s', queue_name)
        self._channel.queue_declare(self.on_queue_declareok, queue_name, durable=True)

    def on_queue_declareok(self, method_frame):
        self.logger.debug('Binding %s to %s with %s',
                          self.exchange, self.queue, self.routing_key)
        # self._channel.queue_bind(self.on_bindok, self.queue,
        #                          self.exchange, self.routing_key)
        self._channel.queue_bind(self.on_bindok, self.queue,
                                 self.exchange)

    def on_bindok(self, unused_frame):
        self.logger.debug('Queue bound')
        self.start_consuming()

    def start_consuming(self):
        self.logger.debug('Issuing consumer related RPC commands')
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         self.queue, )

    def add_on_cancel_callback(self):
        self.logger.debug('Adding consumer cancellation callback')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        self.logger.debug('Consumer was cancelled remotely, shutting down: %r',
                          method_frame)
        if self._channel:
            self._channel.close()

    def on_message(self, channel, basic_deliver, properties, body):
        self.logger.debug('Received message # %s from %s: %s',
                          basic_deliver.delivery_tag, properties.app_id, body)
        try:
            from .callbacks import Callback
            # creating callback object and call its __call__ method via ()
            success = Callback(channel, basic_deliver, properties, body)()
            # ack message if callback was successfull
            if success:
                self.acknowledge_message(basic_deliver.delivery_tag)
        except ImportError:
            # dummy callback just to follow pep8
            class Callback:
                def __init__(self):
                    pass

                def __call__(self):
                    pass
            self.logger.error('Cannot import callback module')

    def acknowledge_message(self, delivery_tag):
        self.logger.debug('Acknowledging message %s', delivery_tag)
        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        if self._channel:
            self.logger.debug('Sending a Basic.Cancel RPC command to RabbitMQ')
            self._channel.basic_cancel(self.on_cancelok, self._consumer_tag)

    def on_cancelok(self, unused_frame):
        self.logger.debug('RabbitMQ acknowledged the cancellation of the consumer')
        self.close_channel()

    def close_channel(self):
        self.logger.debug('Closing the channel')
        self._channel.close()

    def stop(self):
        self.logger.debug('Stopping')
        self._closing = True
        self.stop_consuming()
        self._connection.ioloop.start()
        self.logger.debug('Stopped')

    def close_connection(self):
        self.logger.info('Closing connection')
        self._connection.close()


def publish(name, method, params):
    logger = logging.getLogger(__name__)
    logger.error('Publishing message to {0} {1}'.format(name, params))
    properties = pika.BasicProperties()
    properties.delivery_mode = 2
    properties.headers = {
        'method': method
    }
    Publisher(name)(params, properties=properties)
