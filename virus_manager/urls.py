"""virus_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

from virus_manager.views import *

urlpatterns = [
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico', permanent=True)),
    url(r'^accounts/login/$', LoginView.as_view(), name='login'),
    url(r'^accounts/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^admin/', admin.site.urls),

    url(r'^$', HomePageView.as_view(), name='home'),

    url(r'^hosts/', HostsView.as_view(), name='hosts'),
    url(r'^users/', UsersView.as_view(), name='users'),
    url(r'^viruses/', VirusesView.as_view(), name='viruses'),
    url(r'^files/', FilesView.as_view(), name='files'),
    url(r'^user/(?P<user_id>\d+)', UserDetailView.as_view(), name='user'),
    url(r'^file/(?P<file_id>\d+)', FileDetailView.as_view(), name='file'),
    url(r'^virus/(?P<virus_id>\d+)', VirusDetailView.as_view(), name='virus'),

    url(r'^auth/prefs/$', AuthUserPrefsView.as_view(), name='auth_prefs'),
    url(r'^help/$', HelpView.as_view(), name='help'),
    url(r'^csv/(host|user|virus|file)/$', CsvView.as_view(), name='csv'),
    # API
    url(r'^api/(host|user|virus|file)/(\d+)/', ApiView.as_view(), name='api'),
    url(r'^action/(?P<cathegory>host|user|virus|file)/', ActionView.as_view(), name='action'),

]
