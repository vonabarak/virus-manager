# -*- coding: utf-8 -*-

from django.forms import Form, ModelForm, CharField, PasswordInput, TextInput, Textarea, NumberInput, IntegerField
from virus_manager.models import *

class LoginForm(Form):
    login = CharField(label='Логин')
    password = CharField(max_length=32, widget=PasswordInput, label='Пароль')


class AddHostForm(Form):
    name = CharField(widget=TextInput(), label='Имя хоста')


class EditUserForm(Form):
    name = CharField(widget=TextInput(), label='Имя пользователя')


class HostForm(ModelForm):
    class Meta:
        model = Host
        fields = ['hostname']
        labels = {
            'hostname': u'Имя хоста'
        }


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['host', 'username']
        labels = {
            'host': u'Хост',
            'username': u'Имя пользователя'
        }


class VirusForm(ModelForm):
    class Meta:
        model = Virus
        fields = ['virusname', 'ignoring']
        labels = {
            'virusname': u'Имя вируса',
            'ignoring': u'Игнорирование',
        }


class FileForm(ModelForm):
    class Meta:
        model = File
        fields = ['user', 'filename', 'virus', 'ignoring', 'quarantine', 'deleted']
        labels = {
            'user': u'Пользователь',
            'filename': u'Имя файла',
            'virus': u'Вирус',
            'ignoring': u'Игнорирование',
            'quarantine': u'Карантин',
            'deleted': u'Удалён',
        }


class AddCommentForm(Form):
    comment = CharField(widget=Textarea(), label='Добавить комментарий')
    ticket = IntegerField(widget=NumberInput(), label='Номер тикета', required=False)

class AuthUserPrefsForm(ModelForm):
    class Meta:
        model = AuthUserPrefs
        fields = ['rows_on_page', 'show_create_forms']
        labels = {
            'rows_on_page': u'Строк на странице',
            'show_create_forms': u'Показывать формы добавления объектов'
        }