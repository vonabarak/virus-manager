/**
 * Created by bobr on 6/8/16.
 */

window.FilesToIgnore = {};
window.FilesToQuarantine = {};
window.FilesToDelete = {};


// "scan user's homedir" button in users overview table
$('button[name="scan-user-button"]').click(function(){
    var url = $(this).data("url");
    $.ajax(url + "?scan=true");
    $(this).attr('disabled','disabled');
    setTimeout(function(){window.location.reload();}, 1000);
});

// "scan whole host" button in hosts overview table
$('button[name="scan-host-button"]').click(function(){
    var url = $(this).data("url");
    $.ajax(url + "?scan=true");
    $(this).attr('disabled','disabled');
    setTimeout(function(){window.location.reload();}, 1000);
});

$("[type='checkbox']").bootstrapSwitch({
    size: 'mini',
    onText: $(this).data("on-text"),
    offText: $(this).data("off-text")
});

// tooltips for long filenames
$('[data-toggle="tooltip"]').tooltip({
    container: 'body'
});


$('input[name="ignoring-virus-switch"]').on('switchChange.bootstrapSwitch', function(event, state) {
    var url = $(this).data("url") + "?ignoring=" + state;
    $.ajax(url);
    setTimeout(function(){window.location.reload();}, 1000);
});

// "delete file" button in files overview table
$('button[name="delete-file-button"]').click(function(){
    var url = $(this).data("url") + "?delete=true";
    $.ajax(url);
    $(this).attr('disabled','disabled');
    setTimeout(function(){window.location.reload();}, 1000);
});

$('input[name="delete-file-switch"]').on('switchChange.bootstrapSwitch', function(event, state) {
    window.FilesToDelete[parseInt($(this).data("id"))] = state;
});

$('input[name="ignoring-file-switch"]').on('switchChange.bootstrapSwitch', function(event, state) {
    window.FilesToIgnore[parseInt($(this).data("id"))] = state;
});

$('input[name="quarantine-switch"]').on('switchChange.bootstrapSwitch', function(event, state) {
    window.FilesToQuarantine[parseInt($(this).data("id"))] = state;
});

$('button[name="apply-file-button"]').click(function(){
    $.ajax({
        type: "POST",
        url: $(this).data("url"),
        headers: {'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value},
        data: JSON.stringify({
            "ignoring": window.FilesToIgnore,
            "delete": window.FilesToDelete,
            "quarantine": window.FilesToQuarantine
        }),
        success: function(data, status, jqxhr){ console.log(data, status, jqxhr);},
        dataType: "json",
        contentType: "application/json"
    });
    $(this).attr('disabled','disabled');
    setTimeout(function(){window.location.reload();}, 1000);
});
$('button[name="cancel-file-button"]').click(function(){
    window.location.reload();
});

$('input[name="quarantine-file-switch-all"]').on('switchChange.bootstrapSwitch', function(event, state) {
    var elems = document.getElementsByName("quarantine-switch");
    for (var i=0; i<elems.length; i++) {
        $(elems[i]).bootstrapSwitch('state', state);
    }
});
$('input[name="ignoring-file-switch-all"]').on('switchChange.bootstrapSwitch', function(event, state) {
    var elems = document.getElementsByName("ignoring-file-switch");
    for (var i=0; i<elems.length; i++) {
        $(elems[i]).bootstrapSwitch('state', state);
    }
});
$('input[name="delete-file-switch-all"]').on('switchChange.bootstrapSwitch', function(event, state) {
    var elems = document.getElementsByName("delete-file-switch");
    for (var i=0; i<elems.length; i++) {
        $(elems[i]).bootstrapSwitch('state', state);
    }
});

