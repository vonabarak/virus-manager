#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "virus_manager.settings")

    from django.core.management import execute_from_command_line

    # try:
    #     execute_from_command_line(sys.argv)
    # except KeyboardInterrupt:
    #     if len(sys.argv) >= 2 and sys.argv[1] == 'runserver':
    #         import virus_manager
    #         virus_manager.VMAppConfig.consumer_thread.stop()
    execute_from_command_line(sys.argv)
