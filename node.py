#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pika
import json
import sys

HOSTNAME = "cp403.agava.net"
EXCHANGE = 'virus_manager'
QUEUE = 'virus_manager'
ROUTING_KEY = 'virus_manager'
URL = 'amqp://guest:guest@virus-manager.domain:5672/%2F'


class Publisher:
    def __init__(self):
        self.connection = pika.BlockingConnection(
            pika.URLParameters(URL)
        )
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=QUEUE)
        self.channel.queue_bind(exchange=EXCHANGE,
                                queue=QUEUE,
                                routing_key=ROUTING_KEY)

    def __call__(self, message, *args, **kwargs):
        self.channel.basic_publish(
            exchange=EXCHANGE,
            routing_key=ROUTING_KEY,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2
            )
        )

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()

def main():
    if len(sys.argv) < 5:
        print('Not enough arguments')
        sys.exit(1)
    msg = json.dumps(
        {
           "method": "virus_found",
           "params": {
               "virusname": sys.argv[3],
               "hostname": sys.argv[1],
               "username": sys.argv[2],
               "filename": sys.argv[4]
           }
        }
    )
    # msg = json.dumps(
    #     {
    #        "method": "virus_found",
    #        "params": {
    #            "virusname": "Win32-MegaSupreVirus",
    #            "hostname": HOSTNAME,
    #            "username": "ILoveViruses",
    #            "filename": "path/to/file.php"
    #        }
    #     }
    # )
    Publisher()(msg)

if __name__ == '__main__':
    main()
